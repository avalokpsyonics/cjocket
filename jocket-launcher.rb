#jocket-launcher.rb
#Ultra-lightweight, easily configurable servlet core.

@path = File.expand_path(File.dirname(__FILE__))

require 'yaml'
require 'webrick'
require 'active_support'

def launch(mode)
	#This has to be loaded before the other jocket modules, because some of them inherit from it.
	require "#{@path}/modules/jocket.rb"

	#Require the other modules from jocket/modules
	#Dir.glob(File.join('#{@path}/modules', '**', '*.rb'), &method(:require))
	Dir["#{@path}/modules/*/*.rb"].each {|file| require file }

	#Get the time that the script was initialized
	@time = Time.now

	#Loads an opt hash from a properly formatted .config.yml file
	@opt_hash = YAML.load(File.read("#{@path}/.config.yml"))

	#Sets instance variables based on the values in .config.yml
	@api_key= @opt_hash["api_key"]
	@port = @opt_hash["port"]

	endpoints = {}

	####
	puts "\n ╦╔═╗╔═╗╦╔═╔═╗╔╦╗
 ║║ ║║  ╠╩╗║╣  ║ 
╚╝╚═╝╚═╝╩ ╩╚═╝ ╩ v3.0b"
	
	puts "Loading configuration...\n"
	
	@opt_hash["endpoints"].each do |key,opt|
	    endpoints[key] = Endpoint.new("http://localhost:#{@port}",opt["subdomain"],opt["module"])
	end
	
	puts "Loading configuration...DONE\n"
	
	@jocketServer = WEBrick::HTTPServer.new(:Port => @port)
	trap "INT" do 
	  @jocketServer.shutdown 
	end
	
	puts "Staging endpoints..."
	
	endpoints.each do |key,endpoint|
	  puts "#{key}:\n#{endpoint.show}"

	  if mode == "natpunch"
		  endpoint.bind_hook(@api_key,@port)
	  end
	  	  	  	  
	  endpoint.mount_module(@jocketServer)
	end
	
	puts "Staging endpoints...DONE\n"
	
	puts "Launching the Jocket server on port #{@port}..."
	
	unless mode == "foreground"
		#Starts the WEBrick Daemon service so that the jocketServer will be launched as a daemon.
		WEBrick::Daemon.start
	end

	@jocketServer.start
end
	
def main(args)
	if args[0]
		case args[0]
		when "-f"
			launch("foreground")
			
		when "-n"
			launch("natpunch")

		when "-h"
			puts("Jocket Launcher Options:\n\n-f:    Launch in foreground mode for debugging.\n-n:    Launch in NAT Punch mode.  Requires an ultrahook API key.\n<no args>:    Launch in default mode.\n-h:    Show this screen.\n\n")
		else			
			puts "Invalid flag #{args[0]} passed.  View valid flags with >jocket-launcher -h."
		end
	else
		launch("default")
	end
end

main(ARGV)
