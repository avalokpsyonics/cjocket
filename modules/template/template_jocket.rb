#Template for developing your own custom Jocket modules.

require_relative '../jocket.rb'            #Requires the Jocket class.

class YourCustomJocket < Jocket         #All Jocket modules inherit from the main Jocket class.
    def self.moniker                    #A required method that allows your class to identify itself when called in the abstract.
      "YourCustomJocket"                # Returns the string entered here.
    end

    def do_GET(request,response)        #Basic listener for GET requests.
      puts request
      response.status = 200
    end
  
    def do_POST(request,response)       #Basic listener for POST requests.
      puts request
      response.status = 200
    end
  end
