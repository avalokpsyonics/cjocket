### broker_jocket.rb
#
#  Description: Jocket for parsing and redirecting input to other modules.
#
###

###
#
#  Requirements
#
###

require 'jira-ruby'			#Provides an 'easy to use' interface to the JIRA API
require 'confluence/api/client'		#" for Confluence's API
require 'json'
require_relative '../jocket.rb'            #Requires the Jocket class.

###
#
#  Global declarations
#
###

class BrokerJocket < Jocket         #All Jocket modules inherit from the main Jocket class.
	def self.moniker                    #A required method that allows your class to identify itself when called in the abstract.
		"BrokerJocket"                # Returns the string entered here.
	end

	def config
		{

			:confluence_client => {
				:username => "admin",
		                :password => "password",
				:url => 'https://confluence.yourcompany.biz/'
			},

			:jira_client  => {
				:username => "admin",
		                :password => "password",
                		:site     => 'https://jira.yourcompany.biz',
				:context_path => '',
		                :auth_type => :basic,
                		:read_timeout => 120
			},

			:do_POST  => {
				:services  => {

					"jira_client"  => {

						:broker_operations => {
							"get_issue" => "self.jira_client.Issue.find(parameters['issue_id'])",
							"get_user" => "self.jira_client.User.find(parameters['user'])",
							"save_issue" => "self.jira_client.Issue.find(parameters['issue_id']).save({'fields' => parameters['fields']})"
						}
					},

					"confluence_client" => {

						:broker_operations => {
							"get_page" => "self.confluence_client.get({spaceKey: parameters['space'], title: parameters['page']})"

						}


					}


				}
			}
		}	
		
	end

	def confluence_client

		#Set config context
		config = self.config[:jira_client]

		Confluence::Api::Client.new(config[:username], config[:password], config[:url])

	end

	def jira_client

		#Set config context
		config = self.config[:jira_client]

		JIRA::Client.new(
			{
	        		:username => config[:username],
			        :password => config[:password],
	        		:site     => config[:site],
				:context_path => config[:context_path],
		        	:auth_type => config[:auth_type],
			        :read_timeout => config[:read_timeout]
			}
		)
	end


	def do_POST(request,response)       #Basic listener for POST requests.

		#Set config context
		config = self.config[:do_POST]

		#	Expects a request of the form
		#
		#
		#       {
                #               "service" => "jira_client",
                #               "operation "=> "get_issue",      
                #               "parameters" => {
                #                       "issue_id" => "#{info["issue"]["id"]}"
                #               }
                #       }

		puts "\n::TRUST::BROKER::REQUEST RECEIVED"
		info = JSON.parse!(request.body)
		puts info

		puts "::TRUST::BROKER::PARSING REQUEST..."

		service = info["service"]
		operation = info["operation"]
		parameters = info["parameters"]
		requester = info["requester"]

		puts "::TRUST::BROKER::PARSING REQUEST...DONE"

		#	Allows you to extend the broker with multiple clients.

		puts "::TRUST::BROKER::VALIDATING REQUESTED SERVICE..."
		puts config[:services]
		if config[:services].include?(service)

			puts "::TRUST::BROKER::VALIDATING REQUESTED SERVICE...DONE"

			#Escalate config context
			config = config[:services][service]		

			puts "::TRUST::BROKER::VALIDATING OPERATION..."
			if config[:broker_operations].include?(operation)

				#Escalate config context
				config = config[:broker_operations][operation]

				puts "::TRUST::BROKER::VALIDATING OPERATION...PASS."

				#	DEBUG
				puts "::TRUST::BROKER::PERFORMING OPERATION #{operation} : #{config}"

				#	Performs the whitelisted operation

				eval(config)

				result = JSON.generate(eval(config))

#				puts "THIS IS THE BROKER RESULT: #{result}"

				response.body = result

			else
				puts "::TRUST::BROKER::INVALID OPERATION #{operation} PASSED TO #{service}\n  VALID OPERATIONS:\n  #{config[:broker_operations].keys}"
				response.status = 404
			end
		else
			puts "::TRUST::BROKER::VALIDATING REQUESTED SERVICE...FAIL"
			puts "::TRUST::BROKER::INVALID SERVICE #{service} REQUESTED\n  VALID SERVICES:\n  #{config[:services].keys}"
			response.status = 404
		end

		#puts info
		response.status = 200
	end
end
