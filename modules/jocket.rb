#This is currently just to make things look cleaner, however, any common methods that end up being used by all modules of xJocket type will go here.

class Jocket < WEBrick::HTTPServlet::AbstractServlet  

        def initialize server, mountpoint
                super server
                @mountpoint = mountpoint
        end

end
