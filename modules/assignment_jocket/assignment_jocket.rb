###assignment_jocket.rb
#
#  Description: Jocket for reassigning issues based on transition-linked webhooks sent by JIRA.
#		Essentially allows us to tell JIRA who to assign an issue to based on the data contained within the issue.
#
#  Instructions: Within JIRA, a webhook needs to be set up to point to this jocket
#    The webhook must then be enabled on the appropriate workflow steps as a post-action
#
###

###
#
#  Requirements
#
###

require 'json'				#	Allows us to encode calls to the broker as JSON
require_relative '../jocket.rb'		#	Requires the Jocket class.

###
#
#  Global declarations
#
###

class AssignmentJocket < Jocket		#	All Jocket modules inherit from the main Jocket class.
	def self.moniker                #	A required method that allows your class to identify itself when called in the abstract.
		"AssignmentJocket"	#	Returns the string entered here.
	end

	#	The config method returns a hash containing the data used to drive this Jocket's function.
	#
	#	This is the general layout:
	#	
	#	:schema => {
	#	  Contains the mapping schema used to make assignment decisions for issues.
	#
	#		<PROJECT SLUG> => {
	#		  Contains the mapping schema for a given project
	#
	#			<TRANSITION> => {
	#			  Contains the mapping schema for a given workflow transition
	#
	#				:eval => "<STATEMENT>",
	#				  A statement that when evaluated as code returns a string matching a key in the <TRANSITION> hash
	#
	#				"<KEYS> => "some strings"
	#				  Key(s) that can be returned by eval(:eval), mapped to values that can be used as an input in a broker_transaction
	#			}
	#		}
	#	},
	#
	#	:broker_requests => {
	#	  Contains a listing of operations available for federation through the BrokerJocket.  
	#	  Think of these like "API Aliases."  i.e. :get_user contains the data necessary to process a GET_USER request through the API bridge provided by the BrokerJocket.
	#
	#		:<OPERATION ALIAS> => {
	#		  By convention, this should have a matching gen_<OPERATION ALIAS> method in the main Jocket body.  This contains the evaluation statements and template necessary to generate a request.
	#
	#			:evals => [
	#			  A list of operations that, when executed as code, modify the contents of the :payload key by executing the <STATEMENTS> for each <NAME>.
	#			  These statements are evaluated as a group, not individually.
	#			  Individual statements should be contained within :eval keys as in the :schema[<PROJECT SLUG>][<TRANSITION>] section above.
	#			],
	#
	#			:payload => {
	#			  The data necessary to issue a request to a broker that executes a federated transaction.  Think of these as "arguments" being passed to a Broker-side function.
	#
	#				#########################################
	#				#	THIS GROUPING IS STANDARD	#
	#				#########################################
	#				"requester" => "@mountpoint",	#	All Jocket modules store their configured mountpoints to @mountpoint at load time so that they can communicate internally.
	#				"service" => "jira_client",	#	The name of a Broker-side service, usually an API client of some kind.
	#				"operation"=> "get_issue",	#	The name of a federated operation offered by the selected service.
	#				#########################################
	#				
	#				"parameters" => {
	#				  The set of parameters being passed to the selected operation.
	#
	#					"<NAME>" => "<STATEMENT>",
	#					  <NAME> is the name of the parameter expected by the operation.
	#
	#					  <STATEMENT> is a statement that when evaluated as code will return a string that can be passed directly in an API call.
	#					    This is the statement that the operations contained within :evals will execute.
	#
	#					    Once it has been executed, the associated :eval statement will proceed to assign the return value of <STATEMENT> to <NAME>
	#				}				
	#			}
	#		},
	#
	#		"//Comments can be added within the config hash in this format if necessary." => "",
	#
	#	}
	#

	def config
		{
			:schema => {
				"PROCDEV" => {
                               		"Advance to Pending IT Approval" => {
						:eval => "'user'",
						"user" => "joe"
					},

					"Advance to Finance Review" => {
						:eval => "info['issue']['fields']['customfield_10800']['value']",
						"Project 1" => "carl",
						"Project 2" => "sally"
					},

					"Finance Approves -> Director" => {
						:eval => "info['issue']['fields']['customfield_10800']['value']",
						"Project 1" => "max",
						"Project 2" => "linda"
					}
				}
			},

			:broker_requests => {

				:get_issue => {
					:evals => [
						"broker_request[:payload]['requester'] = eval(broker_request[:payload]['requester'])",
						"broker_request[:payload]['parameters']['issue_id'] = eval(broker_request[:payload]['parameters']['issue_id'])"
					],
					:payload => {
						"requester" => "@mountpoint",
						"service" => "jira_client",
						"operation"=> "get_issue",
						"parameters" => {
							"issue_id" => "info['issue']['id']"
						}
					}
				},

				:get_user => {

					:evals => [
						"broker_request[:payload]['requester'] = eval(broker_request[:payload]['requester'])",
						"broker_request[:payload]['parameters']['user'] = eval(broker_request[:payload]['parameters']['user'])"
					],
						
					:payload => {
						"requester" => "@mountpoint",
						"service" => "jira_client",
						"operation"=> "get_user",
	
						"parameters" => {
							"user" => "self.schema[slug][transition][eval(self.schema[slug][transition][:eval])]",
						}				
					}
				},

				:assign_user => {

					:evals => [
						"broker_request[:payload]['requester'] = eval(broker_request[:payload]['requester'])",
						"broker_request[:payload]['parameters']['issue_id'] = eval(broker_request[:payload]['parameters']['issue_id'])",
						"broker_request[:payload]['parameters']['fields']['assignee']['name'] = eval(broker_request[:payload]['parameters']['fields']['assignee']['name'])"
					],

					:payload => {
						"requester" => "@mountpoint",
						"service" => "jira_client",
						"operation"=> "save_issue",
						"parameters" => {
							"issue_id" => "info['issue']['id']",
							"fields" => {
								"assignee" => {
									"name"=>"user['name']"
								}
							}
						}
					}
				}
			}
		}

	end

	def schema
		
		#Returns the :schema from self.config
		self.config[:schema]	

	end

	def broker_poll(mountpoint,broker_request)
	#	Polls a BrokerJocket running on localhost and returns the response
		JSON.parse!(`curl -H 'Content-Type: application/json' -d '#{broker_request}' #{mountpoint}`)
	end

	def gen_get_issue(info)
		#Builds a broker_request to retrieve an issue object.

		broker_request = self.config[:broker_requests][:get_issue]

                #       Evaluate :eval statements
                #       This is necessary to allow the hash to dynamically calculate key values within the payload
                broker_request[:evals].each do |x|
                        eval(x)
                end

		JSON.generate(broker_request[:payload])
	end

	def gen_get_user(info,slug,transition)

		#Builds a broker request to retrieve a user object.

		broker_request = self.config[:broker_requests][:get_user]

                #       Evaluate :eval statements
                #       This is necessary to allow the hash to dynamically calculate key values within the payload
                broker_request[:evals].each do |x|
                        eval(x)
                end

		JSON.generate(broker_request[:payload])
	end

	def gen_assign_user(info,user)
		#Builds a broker request to assign an issue to a user.

                broker_request = self.config[:broker_requests][:assign_user]

                #       Evaluate :eval statements
                #       This is necessary to allow the hash to dynamically calculate key values within the payload
                broker_request[:evals].each do |x|
                        eval(x)
                end

                JSON.generate(broker_request[:payload])
	end
  

	def do_GET(request,response)        #Basic listener for GET requests.
		puts request
		response.status = 200
	end

	def do_POST(request,response)       #Basic listener for POST requests.
		puts "::TRUST::ASSIGNMENT_JOCKET::WEBHOOK RECEIVED"

		info = JSON.parse!(request.body)
		slug = info["issue"]["fields"]["project"]["key"]
		transition = info["transition"]["transitionName"]

		puts "::TRUST::ASSIGNMENT_JOCKET::GENERATING BROKER REQUEST..."

		broker_request = gen_get_issue(info)

		puts "::TRUST::ASSIGNMENT_JOCKET::GENERATING BROKER REQUEST...DONE"	

		puts "::TRUST::ASSIGNMENT_JOCKET::FORWARDING REQUEST TO BROKER..."
#		puts broker_request

		#	Sends the request for information on the issue ID to the broker and saves the parsed JSON response to a variable
		#	!TODO This could pull from a global index or something.
		issue = broker_poll("http://localhost:7777/jira_broker",broker_request)

		puts "::TRUST::ASSIGNMENT_JOCKET::FORWARDING DATA TO BROKER...DONE"

		puts "::TRUST::ASSIGNMENT_JOCKET::RETRIEVED PAYLOAD FROM BROKER"

		puts "::TRUST::ASSIGNMENT_JOCKET::CHECKING SCHEMA FOR REASSIGNMENT OPERATION..."

		#	Validates that a mapping exists within the schema
		if self.schema.keys.include?(slug) && self.schema[slug].keys.include?(transition)
			puts "::TRUST::ASSIGNMENT_JOCKET::CHECKING SCHEMA FOR REASSIGNMENT OPERATION...TRUE"

			puts "::TRUST::ASSIGNMENT_JOCKET::GENERATING BROKER REQUEST..."	

			#	Generate a broker request for the user data

			broker_request = gen_get_user(info,slug,transition)

			puts "::TRUST::ASSIGNMENT_JOCKET::GENERATING BROKER REQUEST...DONE"	
			puts broker_request

			#	Sends the request for information on the user to the broker and saves the parsed JSON response to a variable
			user = broker_poll("http://localhost:7777/jira_broker",broker_request)
			
			puts "::TRUST::ASSIGNMENT_JOCKET::RETRIEVED PAYLOAD FROM BROKER"

			puts "::TRUST::ASSIGNMENT_JOCKET::GENERATING BROKER REQUEST..."	

			broker_request = gen_assign_user(info,user)

			puts "::TRUST::ASSIGNMENT_JOCKET::GENERATING BROKER REQUEST...DONE"	
			puts broker_request

                    	#       Sends the request for an issue update to the broker and saves the response to a variable.
                        update = broker_poll("http://localhost:7777/jira_broker",broker_request)
                        puts "::TRUST::ASSIGNMENT_JOCKET::RETRIEVED PAYLOAD FROM BROKER"
			puts update
			
			puts "::TRUST::ASSIGNMENT_JOCKET::REASSIGNMENT FROM #{issue["fields"]["assignee"]["name"]} => #{user["name"]} COMPLETE."

		else
			#Fails gracefully
			puts "::TRUST::ASSIGNMENT_JOCKET::CHECKING SCHEMA FOR REASSIGNMENT OPERATION...FALSE"
			response.status = 200

		end				

		response.status = 200
	end
end
