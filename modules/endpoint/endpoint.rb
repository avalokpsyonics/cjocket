#Class that provides the interface for mounting jocket modules onto the server.
#
#Looks like this:
#[server]---[Endpoint---[Module]]

#Lets us use the :classify method
require 'active_support/core_ext/string/inflections.rb'

class Endpoint
    def initialize(base_uri,subdomain,endpoint_module)
      @base_uri = base_uri
      @subdomain = subdomain

      #Takes the string passed as the endpoint_module parameter, turns it into a class, and stores it so that it can be called directly.
      @module = endpoint_module.classify.constantize
    end
  
    def show
      "  Subdomain: #{@subdomain}\n  Module: #{@module.moniker}"
    end
  
    def mount_module(server)
      puts "    Mounting #{@module.moniker} on /#{@subdomain}..."

#	Working
#      server.mount("/#{@subdomain}",@module)

      server.mount("/#{@subdomain}",@module,"#{@base_uri}/#{@subdomain}")	#<--

      puts "    Mounting #{@module.moniker} on /#{@subdomain}...DONE"
    end
  
    def bind_hook(key,port)
      puts "    Spawning ultrahook bound to /#{@subdomain}..."

      #Detaches the ultrahook process so that the main ruby thread can continue
      Process.detach(
        #Creates a new instance of the ultrahook process
        # !TODO make the local port configurable
        Process.spawn("ultrahook -k #{key} #{@subdomain} http://localhost:#{port}/#{@subdomain} >> .msg") & sleep(1)	#<--- see below.
      )
        File.readlines(".msg").each {|l| puts "      #{l}"}	#<--- "
        File.delete(".msg")					#<--- "

	###
	#  OK.  I am FULLY aware that this is a hackjob.  I have spent *7 hours* trying to figure out a way to redirect startup output from a detached process to STDOUT "the right way."
  #
	#  Without this hack in place, all of the STDOUT writes from the triggered ultrahook processes get printed to the screen AFTER the server starts.
	#  IO.popen / Open3 / PTY.spawn(foo).join / several others I can't remember don't work for this particular application.
	#  Apparently nobody's EVER wanted to print the initial output from a long-running daemonized process before returning to the main thread IN THE HISTORY OF COMPUTING.
	#
	#  What this does is pipes the first second of STDOUT from the spawned process to a file called .msg, then detaches the spawned process, clearing the block.
  #  Next, the contents of .msg are preprocessed and printed to the TTY manually
  #  Finally, the .msg file is deleted.
  #
  #  Basically, .msg flickers in and out 
  #  as a buffer that can be accessed 
  #  in a SANE AND REASONABLE MANNER BY MORTALS.
  #
  #  If you know how to do this properly by jockeying IO pipes, please let me know- I'm genuinely interested.  
  #  
	###

     puts "    Spawning ultrahook bound to /#{@subdomain}...DONE"
    end
  end
