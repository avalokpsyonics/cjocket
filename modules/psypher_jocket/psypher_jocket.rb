#Jocket module for generating passwords without special characters in multiple languages.

require_relative '../jocket.rb'            #Requires the Jocket class.

class PsypherJocket < Jocket         #All Jocket modules inherit from the main Jocket class.
    def self.moniker                    #A required method that allows your class to identify itself when called in the abstract.
      "PsypherJocket"                # Returns the string entered here.
    end

    def do_GET(request,response)        #Basic listener for GET requests.
      status, content_type, body = gen_psypher(request)
      response.status = status
      response['Content-Type'] = "text/plain"
      response.body = body
    end

    def gen_psypher(request)
      puts request

      swordlist = []

      File.open("/home/anubis/utils/jocket/modules/psypher_jocket/spanish.txt", "r") do |f|
        f.each_line do |line|
            swordlist.push(line.chomp)
        end
      end

      fwordlist = []

      File.open("/home/anubis/utils/jocket/modules/psypher_jocket/french.txt", "r") do |f|
        f.each_line do |line|
            fwordlist.push(line.chomp)
        end
      end

      ewordlist = []

      File.open("/home/anubis/utils/jocket/modules/psypher_jocket/english.txt", "r") do |f|
        f.each_line do |line|
            ewordlist.push(line.chomp)
        end
      end


      body = "\nSPANISH--> \n#{swordlist.sample(4).join(" ")}\n\nFRENCH--> \n#{fwordlist.sample(4).join(" ")}\n\nENGLISH--> \n#{ewordlist.sample(4).join(" ")}\n\n"

      return 200, "text/html", body

    end
  
    def do_POST(request,response)       #Basic listener for POST requests.
      puts request

    end

end
